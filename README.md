# README #
# dev-desktop

This README documents the steps necessary to setup a Moth Group developer.

### What is this repository for? ###

+ Sets up a developer with;
    * git
    * vim with plugins
    * docker
    * npm
    * TypeScript
    * groovy
    * gradle
    * gradle
    * spock
    * geb

* Version
0.1.1

### How do I get set up? ###

* Summary of set up

    clone this repository:
    cd git
    git clone git@bitbucket.org:mothballs/dev-desktop.git

* Configuration:

		git config --global user.email "percy.moth@gmail.com"
		git config --global user.email "Percy Moth"
		git config --global core.editor "vim"

* Dependencies:

		sudo apt install vim

* Deployment instructions:

		git.setup
		vim.setup


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: percy.moth@gmail.com
